package com.idfy.videocomponent;

import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import com.idfy.core.BackPressEvent;
import org.json.JSONObject;

/**
 * This class do the overall VideoComponent process.
 * create a instance of this class in your activity or Fragment.
 * with the instance call the {@link #init(String, FragmentManager, int, CallBack)}
 */

public class VideoComponent extends BackPressEvent {
    private CameraViewFragment fragment;

    public VideoComponent() {
    }

    /**
     * This method needs to be called on back press event from the parent class to send the back press event in video component.
     * @return boolean<Br>
     * true: The back press needs to be handled by the parent.<Br>
     * false: The child has handled the back press event.
     */
    @Override
    public boolean onBackPress() {
        return fragment.onBackPress();
    }

    /**
     * This Method should be called first to initiate the VideoComponent process,
     * To initiate pass the below params.
     *
     * @param tag             tag for handling Fragment Backstack.
     * @param fragmentManager activity fragmentManager or child fragment manager.
     * @param containerId     layout id where the Ui should be shown.
     * @param callBack        callback for success, intermediate or failure.
     */
    public void init(String tag, FragmentManager fragmentManager, int containerId, CallBack callBack) {

        fragment = CameraViewFragment.newInstance();
        FragmentTransaction childFragTrans = fragmentManager.beginTransaction();
        childFragTrans.replace(containerId, fragment, tag);
        //childFragTrans.replace(R.id.content_layout, fragment, tag);
        childFragTrans.addToBackStack(tag);
        childFragTrans.commit();
        fragment.initCallBack(new CameraViewFragment.VideoComponentCallBack() {
            @Override
            public void onVCSuccess(JSONObject object) {
                callBack.onVCSuccess(object);
            }

            @Override
            public void onVCIntermediate(JSONObject object) {
                callBack.onVCIntermediate(object);

            }

            @Override
            public void onVCFailure(JSONObject object) {
                callBack.onVCFailure(object);
            }
        });
    }

    /**
     * This Method is used to connect to the MSSocket, pass the below required arguments.
     *
     * @param roomId room Id of the media server.
     * @param participantId participant Id of the user to connect the room.
     */
    public void setupMSSocket(String roomId, String participantId) {
        if (fragment != null) {
            fragment.setupMSSocket(roomId, participantId);
        }
    }

    /**
     * This Method is used to clean up the MSSocket and Videocomponents Instances.
     */
    public void cleanUp() {
        if (fragment != null) {
            fragment.cleanUp();
        }
    }

    /**
     * This Method is used to take the Screenshot and return the screenshot path in callback.
     */
    public void takePicture() {
        if (fragment != null) {
            fragment.takePicture();
        }
    }

    /**
     * This method is used to show the connecting dialog on the ui.
     *
     * @param message Message that needs to be displayed on the connecting dialog.
     */
    public void showConnectingDialog(String message) {
        if (fragment != null) {
            fragment.showConnectingDialog(message);
        }
    }

    /**
     * Pass this CallBack instance to the {@link #init(String, FragmentManager, int, CallBack)}.
     */
    public interface CallBack {
        void onVCSuccess(JSONObject object);

        void onVCIntermediate(JSONObject object);

        void onVCFailure(JSONObject object);
    }
}
