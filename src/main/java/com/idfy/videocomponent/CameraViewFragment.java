package com.idfy.videocomponent;

import static android.content.Context.MEDIA_PROJECTION_SERVICE;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.ImageFormat;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.SurfaceTexture;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CameraMetadata;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.CaptureResult;
import android.hardware.camera2.TotalCaptureResult;
import android.hardware.camera2.params.MeteringRectangle;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.media.AudioManager;
import android.media.ExifInterface;
import android.media.Image;
import android.media.ImageReader;
import android.media.MediaRecorder;
import android.media.projection.MediaProjection;
import android.media.projection.MediaProjectionManager;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.CountDownTimer;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.os.SystemClock;
import android.transition.Slide;
import android.transition.Transition;
import android.transition.TransitionManager;
import android.util.Log;
import android.util.Size;
import android.util.SparseIntArray;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.OrientationEventListener;
import android.view.ScaleGestureDetector;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.idfy.core.BackPressEvent;
import com.idfy.core.components.Components;
import com.idfy.msadaptor.MsAdaptor;
import com.idfy.msadaptor.MsAdaptorCallBack;
import com.idfy.msadaptor.model.NetworkSignalModel;
import com.idfy.core.DALCapture;
import com.idfy.core.model.ThemeConfig;
import com.idfy.core.api.ApiCallBack;
import com.idfy.core.socket.SocketCallback;
import com.idfy.videocomponent.adapter.NetworkSignalAdapter;
import com.idfy.videocomponent.util.AspectRatio;
import com.idfy.videocomponent.util.AutoFitTextureView;
import com.idfy.videocomponent.util.AutoFocusHelper;
import com.idfy.videocomponent.util.CameraConstants;
import com.idfy.videocomponent.util.CameraUtil;
import com.idfy.videocomponent.util.ConnectionIndicator;
import com.idfy.videocomponent.util.Utility;
import com.idfy.videocomponent.util.FocusView;
import com.idfy.videocomponent.util.SizeMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.webrtc.EglBase;
import org.webrtc.ScreenCapturerAndroid;
import org.webrtc.SurfaceViewRenderer;
import org.webrtc.VideoCapturer;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.SortedSet;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link CameraViewFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CameraViewFragment extends BackPressEvent implements SocketCallback {
    private static final int SENSOR_ORIENTATION_DEFAULT_DEGREES = 90;
    private static final int SENSOR_ORIENTATION_INVERSE_DEGREES = 270;

    private static final SparseIntArray DEFAULT_ORIENTATIONS = new SparseIntArray();
    private static final SparseIntArray INVERSE_ORIENTATIONS = new SparseIntArray();

    private static final SparseIntArray INTERNAL_FACINGS = new SparseIntArray();

    static {
        INTERNAL_FACINGS.put(CameraConstants.FACING_BACK, CameraCharacteristics.LENS_FACING_BACK);
        INTERNAL_FACINGS.put(CameraConstants.FACING_FRONT, CameraCharacteristics.LENS_FACING_FRONT);
    }

    private static final int REQUEST_CAMERA_PERMISSION = 1;

    private static final String FRAGMENT_DIALOG = "dialog";

    private static final String[] VIDEO_PERMISSIONS = {
            Manifest.permission.CAMERA,
            Manifest.permission.RECORD_AUDIO,
    };

    static {
        DEFAULT_ORIENTATIONS.append(Surface.ROTATION_0, 90);
        DEFAULT_ORIENTATIONS.append(Surface.ROTATION_90, 0);
        DEFAULT_ORIENTATIONS.append(Surface.ROTATION_180, 270);
        DEFAULT_ORIENTATIONS.append(Surface.ROTATION_270, 180);
    }

    static {
        INVERSE_ORIENTATIONS.append(Surface.ROTATION_0, 270);
        INVERSE_ORIENTATIONS.append(Surface.ROTATION_90, 180);
        INVERSE_ORIENTATIONS.append(Surface.ROTATION_180, 90);
        INVERSE_ORIENTATIONS.append(Surface.ROTATION_270, 0);
    }

    /**
     * Tag for the {@link Log}.
     */
    private static final String TAG = "Camera2Fragment";

    /**
     * Camera state: Showing camera preview.
     */
    private static final int STATE_PREVIEW = 0;

    /**
     * Camera state: Waiting for the focus to be locked.
     */
    private static final int STATE_WAITING_LOCK = 1;

    /**
     * Camera state: Waiting for the exposure to be precapture state.
     */
    private static final int STATE_WAITING_PRECAPTURE = 2;

    /**
     * Camera state: Waiting for the exposure state to be something other than precapture.
     */
    private static final int STATE_WAITING_NON_PRECAPTURE = 3;

    /**
     * Camera state: Picture was taken.
     */
    private static final int STATE_PICTURE_TAKEN = 4;

    /**
     * Max preview width that is guaranteed by Camera2 API
     */
    private static final int MAX_PREVIEW_WIDTH = 1920;

    /**
     * Max preview height that is guaranteed by Camera2 API
     */
    private static final int MAX_PREVIEW_HEIGHT = 1080;


    private static final int MSG_CAPTURE_PICTURE_WHEN_FOCUS_TIMEOUT = 100;

    private Rect mCropRegion;

    private MeteringRectangle[] mAFRegions = AutoFocusHelper.getZeroWeightRegion();

    private MeteringRectangle[] mAERegions = AutoFocusHelper.getZeroWeightRegion();

    private AspectRatio mAspectRatio = CameraConstants.DEFAULT_ASPECT_RATIO;

    private final SizeMap mPreviewSizes = new SizeMap();

    /**
     * ID of the current {@link CameraDevice}.
     */
    private String mCameraId;

    /**
     * An {@link AutoFitTextureView} for camera preview.
     */
    public AutoFitTextureView mTextureView;

    /**
     * The view for manual tap to focus
     */
    public FocusView mFocusView;

    /**
     * A {@link CameraCaptureSession } for camera preview.
     */
    private CameraCaptureSession mPreviewSession;

    /**
     * A reference to the opened {@link CameraDevice}.
     */
    private CameraDevice mCameraDevice;


    /**
     * {@link CaptureRequest.Builder} for the camera preview
     */
    private CaptureRequest.Builder mPreviewRequestBuilder;

    /**
     * {@link CaptureRequest} generated by {@link #mPreviewRequestBuilder}
     */
    private CaptureRequest mPreviewRequest;


    private CameraCharacteristics mCameraCharacteristics;

    /**
     * The current state of camera state for taking pictures.
     *
     * @see #mCaptureCallback
     */
    private int mState = STATE_PREVIEW;

    /**
     * A {@link Semaphore} to prevent the app from exiting before closing the camera.
     */
    private Semaphore mCameraOpenCloseLock = new Semaphore(1);

    /**
     * The current camera auto focus mode
     */
    private boolean mAutoFocus = true;

    /**
     * Whether the current camera device supports auto focus or not.
     */
    private boolean mAutoFocusSupported = true;

    /**
     * The current camera flash mode
     */
    private int mFlash = CameraConstants.FLASH_AUTO;

    /**
     * Whether the current camera device supports flash or not.
     */
    private boolean mFlashSupported = true;

    /**
     * The current camera facing mode
     */
    private int mFacing = CameraConstants.FACING_FRONT;

    /**
     * Whether the current camera device can switch back/front or not.
     */
    private boolean mFacingSupported = true;

    /**
     * Orientation of the camera sensor
     */
    private int mSensorOrientation;

    /**
     * The {@link Size} of camera preview.
     */
    private Size mPreviewSize;

    /**
     * The {@link Size} of video recording.
     */
    private Size mVideoSize;

    /**
     * MediaRecorder
     */
    private MediaRecorder mMediaRecorder;

    /**
     * Whether the camera is recording video now
     */
    private boolean mIsRecordingVideo;

    /**
     * Whether the camera is manual focusing now
     */
    private boolean mIsManualFocusing;

    /**
     * An additional thread for running tasks that shouldn't block the UI.
     */
    private HandlerThread mBackgroundThread;

    /**
     * A {@link Handler} for running tasks in the background.
     */
    private Handler mBackgroundHandler;

    /**
     * An {@link ImageReader} that handles still image capture.
     */
    private ImageReader mImageReader;

    /**
     * The output file path of video recording.
     */
    private String mNextVideoAbsolutePath;

    /**
     * The output file path of take picture.
     */
    private String mNextPictureAbsolutePath;
    private MsAdaptor msAdaptor;
    private ImageButton btnMute, btnSwitch;
    private ImageView btnPublish;
    private Spinner spiBitRate;
    private SurfaceViewRenderer remoteViewRenderer;
    private EglBase eglBase = EglBase.create();
    private String hostIp = "";
    private String roomId = "";
    boolean isZoomInProg = false;
    public float finger_spacing = 0;
    public double zoom_level = 1;
    public static final String CAMERA_FRONT = "1";
    public static final String CAMERA_BACK = "0";
    private CameraManager manager;
    private Button btnScreenshot;
    private boolean isSpeaker = true;
    private boolean isMute = true;
    private AudioManager audioManager;
    private ImageButton btnSpeaker;
    private VideoCapturer capturer;
    private boolean isScreenShared = false;
    boolean isPublish = true;
    boolean isFirstRun = true;
    private Button noAudio;
    private Button noVideo;
    private boolean isHealthChecked = false;
    private LinearLayout waitingRoomLayout;
    private LottieAnimationView imgWifiLogo;
    private LottieAnimationView progressBar;
    private TextView txtInfoTitle;
    private TextView txtInfoSub;
    private FloatingActionButton fabCameraSwitch;
    private FloatingActionButton fabHamburger;
    private FloatingActionButton fabMic;
    private FloatingActionButton fabVideoSwitch;
    private FloatingActionButton fabEndCall;
    private TextView txtCantSee;
    private TextView txtCantHear;
    private Animation fab_hamburger_open, fab_hamburger_close;
    private Boolean isHamBurgerOpen = false;
    private static Intent mMediaProjectionPermissionResultData;
    private List<NetworkSignalModel> networkSignalModelList;
    private RecyclerView rcViewNetwork;
    private NetworkSignalAdapter networkSignalAdapter;
    private static final int AVERAGE_NETWORK_THRESHOLD = 50;
    private static final int STRONG_NETWORK_THRESHOLD = 100;
    private boolean isReconnecting = false;
    private boolean isMicMuted;
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM = "PARTICIPANT_ID";
    private static final String ARG_PARAM2 = "ROOM_ID";
    // TODO: Rename and change types of parameters
    private String token;
    private String sessionId;
    private String requestId;
    private String sessionToken;
    private DALCapture dalCapture;
    private String networkID;
    private JSONObject networkConfigObject;
    private boolean isNetworkCheckNeeded = false;
    private boolean isRoomJoinNeeded = false;
    private int timeOut = 0;
    private Timer healthCheckTimer;
    private ConstraintLayout cameraRootLayout;
    private Components uiComponents;
    private int reconnectingId;
    private AlertDialog alertDialog;
    private View view;
    private RelativeLayout infoMainLayout;
    private RelativeLayout callEndedLayout;
    private View layNetworkStateFail;
    private CountDownTimer cTimer = null;
    private String participantId = "";
    private String socketRoomId;
    private String streamReferenceId;
    private String agentStreamId;
    private ConnectionIndicator connectionIndicator;
    private ThemeConfig themeConfig;
    private RelativeLayout rlControl;
    private Executor executor = Executors.newSingleThreadExecutor();
    private static final String FRAGMENT_REQUEST = "CAMERA_VIEW_CALLBACK";
    private float zoom = 1.f;
    private float maxZoom = 2.f;
    private int rotationAngle = 0;
    private long mLastClickTime;
    private VideoComponentCallBack videoComponentCallBack;


    /**
     * {@link TextureView.SurfaceTextureListener} handles several lifecycle events on a
     * {@link TextureView}.
     */
    private final TextureView.SurfaceTextureListener mSurfaceTextureListener
            = new TextureView.SurfaceTextureListener() {

        @Override
        public void onSurfaceTextureAvailable(SurfaceTexture texture, int width, int height) {
            openCamera(width, height);
        }

        @Override
        public void onSurfaceTextureSizeChanged(SurfaceTexture texture, int width, int height) {
            configureTransform(width, height);
        }

        @Override
        public boolean onSurfaceTextureDestroyed(SurfaceTexture texture) {
            if (mTextureView != null) {
                mTextureView.setSurfaceTextureListener(null);
            }
            return true;
        }

        @Override
        public void onSurfaceTextureUpdated(SurfaceTexture texture) {
        }

    };
    private int mMediaProjectionPermissionResultCode;


    /**
     * {@link CameraDevice.StateCallback} is called when {@link CameraDevice} changes its state.
     */
    private final CameraDevice.StateCallback mStateCallback = new CameraDevice.StateCallback() {

        @Override
        public void onOpened(@NonNull CameraDevice cameraDevice) {
            // This method is called when the camera is opened.  We start camera preview here.
            mCameraOpenCloseLock.release();
            mCameraDevice = cameraDevice;
            createCameraPreviewSession();
        }

        @Override
        public void onDisconnected(@NonNull CameraDevice cameraDevice) {
            mCameraOpenCloseLock.release();
            cameraDevice.close();
            mCameraDevice = null;
        }

        @Override
        public void onError(@NonNull CameraDevice cameraDevice, int error) {
            mCameraOpenCloseLock.release();
            cameraDevice.close();
            mCameraDevice = null;
            Activity activity = getActivity();
            if (null != activity) {
                showToast("Camera is error: " + error);
                activity.finish();
            }
        }

    };


    /**
     * This a callback object for the {@link ImageReader}. "onImageAvailable" will be called when a
     * still image is ready to be saved.
     */
    private final ImageReader.OnImageAvailableListener mOnImageAvailableListener
            = new ImageReader.OnImageAvailableListener() {

        @Override
        public void onImageAvailable(ImageReader reader) {
            mNextPictureAbsolutePath = getPictureFilePath(getActivity());
            //mBackgroundHandler.post(new ImageSaver(reader.acquireNextImage(), new File(mNextPictureAbsolutePath)));
            //  showToast("Picture saved: " + mNextPictureAbsolutePath);
            Log.i(TAG, "Picture saved: " + mNextPictureAbsolutePath);


            new Thread(new Runnable() {
                @Override
                public void run() {
                    Image mImage = reader.acquireNextImage();
                    ByteBuffer buffer = mImage.getPlanes()[0].getBuffer();
                    byte[] bytes = new byte[buffer.remaining()];
                    buffer.get(bytes);
                    FileOutputStream output = null;
                    try {
                        output = new FileOutputStream(new File(mNextPictureAbsolutePath));
                        output.write(bytes);
                    } catch (IOException e) {
                        e.printStackTrace();
                    } finally {
                        mImage.close();
                        if (null != output) {
                            try {
                                output.close();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Bitmap bitmap = BitmapFactory.decodeFile(mNextPictureAbsolutePath);
                            try {
                                ExifInterface exif = new ExifInterface(mNextPictureAbsolutePath);
                                int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);

                                //Log.d("EXIF", "Exif: " + orientation + " Angle: " + rotationAngle);
                                Matrix matrix = new Matrix();
                                if (orientation == 6) {
                                    matrix.postRotate(90 + rotationAngle);
                                } else if (orientation == 3) {
                                    matrix.postRotate(180 + rotationAngle);
                                } else if (orientation == 8) {
                                    matrix.postRotate(270 + rotationAngle);
                                } else {
                                    matrix.postRotate(rotationAngle);
                                }

                                bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true); // rotating bitmap
                                if (rotationAngle == 0 || rotationAngle == 180) {
                                    // portrait
                                    bitmap = Bitmap.createScaledBitmap(bitmap, 480, 640, false);
                                } else {
                                    // landscape
                                    bitmap = Bitmap.createScaledBitmap(bitmap, 640, 480, false);
                                }


                            } catch (Exception e) {
                            }
                            String path = Utility.getUriPath(getActivity(), bitmap);
                            //  Toast.makeText(getActivity(), "Screenshot " + path, Toast.LENGTH_SHORT).show();
                            new File(mNextPictureAbsolutePath).delete();
                            JSONObject object = new JSONObject();
                            try {
                                object.put("event", "UPLOAD_SCREENSHOT");
                                object.put("path", path);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            videoComponentCallBack.onVCIntermediate(object);
                            //uploadScreenShot(artifact, link, path);
                        }
                    });
                }
            }).start();

        }

    };


    /**
     * A {@link CameraCaptureSession.CaptureCallback} that handles events related to JPEG capture.
     */
    private final CameraCaptureSession.CaptureCallback mCaptureCallback
            = new CameraCaptureSession.CaptureCallback() {

        private void process(CaptureResult result) {
            //  Log.i(TAG, "CaptureCallback mState: " + mState);
            switch (mState) {
                case STATE_PREVIEW: {
                    // We have nothing to do when the camera preview is working normally.
                    break;
                }
                case STATE_WAITING_LOCK: {
                    Integer afState = result.get(CaptureResult.CONTROL_AF_STATE);
                    Log.i(TAG, "STATE_WAITING_LOCK afState: " + afState);
                    if (afState == null) {
                        mState = STATE_PICTURE_TAKEN;
                        captureStillPicture();
                    } else if (CaptureResult.CONTROL_AF_STATE_FOCUSED_LOCKED == afState ||
                            CaptureResult.CONTROL_AF_STATE_NOT_FOCUSED_LOCKED == afState) {
                        // CONTROL_AE_STATE can be null on some devices
                        Integer aeState = result.get(CaptureResult.CONTROL_AE_STATE);
                        if (aeState == null ||
                                aeState == CaptureResult.CONTROL_AE_STATE_CONVERGED) {
                            mState = STATE_PICTURE_TAKEN;
                            captureStillPicture();
                        } else {
                            runPrecaptureSequence();
                        }
                    }
                    break;
                }
                case STATE_WAITING_PRECAPTURE: {
                    // CONTROL_AE_STATE can be null on some devices
                    Integer aeState = result.get(CaptureResult.CONTROL_AE_STATE);
                    if (aeState == null ||
                            aeState == CaptureResult.CONTROL_AE_STATE_PRECAPTURE ||
                            aeState == CaptureRequest.CONTROL_AE_STATE_FLASH_REQUIRED) {
                        mState = STATE_WAITING_NON_PRECAPTURE;
                    }
                    break;
                }
                case STATE_WAITING_NON_PRECAPTURE: {
                    // CONTROL_AE_STATE can be null on some devices
                    Integer aeState = result.get(CaptureResult.CONTROL_AE_STATE);
                    if (aeState == null || aeState != CaptureResult.CONTROL_AE_STATE_PRECAPTURE) {
                        mState = STATE_PICTURE_TAKEN;
                        captureStillPicture();
                    }
                    break;
                }
            }
        }

        @Override
        public void onCaptureProgressed(@NonNull CameraCaptureSession session,
                                        @NonNull CaptureRequest request,
                                        @NonNull CaptureResult partialResult) {
            process(partialResult);
        }

        @Override
        public void onCaptureCompleted(@NonNull CameraCaptureSession session,
                                       @NonNull CaptureRequest request,
                                       @NonNull TotalCaptureResult result) {
            process(result);
        }

    };

    /**
     * Shows a {@link Toast} on the UI thread.
     *
     * @param text The message to show
     */
    private void showToast(final String text) {
        final Activity activity = getActivity();
        if (activity != null) {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(activity, text, Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    /**
     * We choose a largest picture size with mAspectRatio
     */
    Size choosePictureSize(Size[] choices) {
        List<Size> pictureSizes = Arrays.asList(choices);
        Collections.sort(pictureSizes, new CompareSizesByArea());
        int maxIndex = pictureSizes.size() - 1;
        for (int i = maxIndex; i >= 0; i--) {
            if (pictureSizes.get(i).getWidth() == pictureSizes.get(i).getHeight() *
                    mAspectRatio.getX() / mAspectRatio.getY()) {
                return pictureSizes.get(i);
            }
        }
        return pictureSizes.get(maxIndex);
    }

    /**
     * We choose a largest video size with mAspectRatio. Also, we don't use sizes
     * larger than 1080p, since MediaRecorder cannot handle such a high-resolution video.
     *
     * @param choices The list of available sizes
     * @return The video size
     */
    Size chooseVideoSize(Size[] choices) {
        List<Size> videoSizes = Arrays.asList(choices);
        List<Size> supportedVideoSizes = new ArrayList<>();
        Collections.sort(videoSizes, new CompareSizesByArea());
        for (int i = videoSizes.size() - 1; i >= 0; i--) {
            if (videoSizes.get(i).getWidth() <= MAX_PREVIEW_WIDTH &&
                    videoSizes.get(i).getHeight() <= MAX_PREVIEW_HEIGHT) {
                supportedVideoSizes.add(videoSizes.get(i));
                if (videoSizes.get(i).getWidth() == videoSizes.get(i).getHeight() *
                        mAspectRatio.getX() / mAspectRatio.getY()) {
                    return videoSizes.get(i);
                }
            }
        }
        return supportedVideoSizes.size() > 0 ? supportedVideoSizes.get(0) : choices[choices.length - 1];
    }

    /**
     * Given {@code choices} of {@code Size}s supported by a camera, choose the smallest one that
     * is at least as large as the respective texture view size, and that is at most as large as the
     * respective max size, and whose aspect ratio matches with the specified value. If such size
     * doesn't exist, choose the largest one that is at most as large as the respective max size,
     * and whose aspect ratio matches with the specified value.
     *
     * @param choices           The list of sizes that the camera supports for the intended output
     *                          class
     * @param textureViewWidth  The width of the texture view relative to sensor coordinate
     * @param textureViewHeight The height of the texture view relative to sensor coordinate
     * @param maxWidth          The maximum width that can be chosen
     * @param maxHeight         The maximum height that can be chosen
     * @return The optimal {@code Size}, or an arbitrary one if none were big enough
     */
    Size chooseOptimalSize(Size[] choices, int textureViewWidth,
                           int textureViewHeight, int maxWidth, int maxHeight) {
        mPreviewSizes.clear();
        // Collect the supported resolutions that are at least as big as the preview Surface
        List<Size> bigEnough = new ArrayList<>();
        // Collect the supported resolutions that are smaller than the preview Surface
        List<Size> notBigEnough = new ArrayList<>();
        int w = mAspectRatio.getX();
        int h = mAspectRatio.getY();
        for (Size option : choices) {
            if (option.getWidth() <= maxWidth && option.getHeight() <= maxHeight) {
                mPreviewSizes.add(new com.idfy.videocomponent.util.Size(option.getWidth(), option.getHeight()));
                if (option.getHeight() == option.getWidth() * h / w) {
                    if (option.getWidth() >= textureViewWidth &&
                            option.getHeight() >= textureViewHeight) {
                        bigEnough.add(option);
                    } else {
                        notBigEnough.add(option);
                    }
                }
            }
        }

        // Pick the smallest of those big enough. If there is no one big enough, pick the
        // largest of those not big enough.
        if (bigEnough.size() > 0) {
            return Collections.min(bigEnough, new CompareSizesByArea());
        } else if (notBigEnough.size() > 0) {
            return Collections.max(notBigEnough, new CompareSizesByArea());
        } else {
            Log.e(TAG, "Couldn't find any suitable preview size");
            mAspectRatio = AspectRatio.of(4, 3);
            SortedSet<com.idfy.videocomponent.util.Size> sortedSet = mPreviewSizes.sizes(mAspectRatio);
            if (sortedSet != null) {
                com.idfy.videocomponent.util.Size lastSize = sortedSet.last();
                return new Size(lastSize.getWidth(), lastSize.getHeight());
            }
            mAspectRatio = AspectRatio.of(choices[0].getWidth(), choices[0].getHeight());
            return choices[0];
        }
    }

    private void detectOrientation() {
        OrientationEventListener mOrientationListener = new OrientationEventListener(
                getActivity()) {
            @Override
            public void onOrientationChanged(int orientation) {

                if (orientation == -1) {
                    // flat facing on ground
                    rotationAngle = 270;
                } else if (orientation >= 315 || orientation < 45) {
                    rotationAngle = 0;
                } else if (orientation >= 45 && orientation < 135) {
                    rotationAngle = 90;
                } else if (orientation >= 135 && orientation < 225) {
                    rotationAngle = 180;
                } else if (orientation >= 225 && orientation < 315) {
                    rotationAngle = 270;
                }
                //Log.d("EXIF", "Orientation: " + orientation + " Angle: " + rotationAngle);
            }
        };
        if (mOrientationListener.canDetectOrientation()) {
            mOrientationListener.enable();
        }
    }

    public void initCallBack(VideoComponentCallBack videoComponentCallBack) {
        this.videoComponentCallBack = videoComponentCallBack;
    }

    public interface VideoComponentCallBack {
        void onVCSuccess(JSONObject object);

        void onVCIntermediate(JSONObject object);

        void onVCFailure(JSONObject object);
    }

    public void setupMSSocket(String roomId, String participantId) {
        if (msAdaptor != null) {
            msAdaptor.setUpSocket(roomId, participantId, CameraViewFragment.this);
        }
    }


    public CameraViewFragment() {
        // Required empty public constructor
    }

    @Override
    public boolean onBackPress() {
        showExitDialog();
        return true;
    }

    /**
     * This method initiate the video Component View  and return the CameraViewFragment Instance
     *
     * @return fragment
     */

    public static CameraViewFragment newInstance() {
        CameraViewFragment fragment = new CameraViewFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        msAdaptor = new MsAdaptor();
        uiComponents = new Components(requireActivity());
        dalCapture = DALCapture.getInstance();
        themeConfig = dalCapture.getCaptureTheme();
        token = dalCapture.getToken();
        sessionId = dalCapture.getSessionId();
        requestId = dalCapture.getRequestId();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.activity_camera_view, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.view = view;
        initView(view);
        btnCallControl(View.GONE);
        detectOrientation();
        // initOnBackPressed();
    }

    AlertDialog alertEndCallDialog;

    private void initOnBackPressed() {
        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (cameraRootLayout.getVisibility() == View.VISIBLE) {

                    if (keyCode == KeyEvent.KEYCODE_BACK) {
                        System.out.println("Show Popup for exit");
                        showExitDialog();
                        return true;
                    }
                }
                return false;
            }
        });
    }

    private void sendEndCallTOBaseFragment() {
        JSONObject object = new JSONObject();
        try {
            object.put("event", "CALL_END");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        videoComponentCallBack.onVCSuccess(object);
    }

    private void showExitDialog() {

        if (alertEndCallDialog == null) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            ViewGroup viewGroup = view.findViewById(android.R.id.content);
            View dialogView = LayoutInflater.from(getActivity()).inflate(R.layout.end_call_dialog_layout, viewGroup, false);
            RelativeLayout endRelativeLayout = dialogView.findViewById(R.id.warning_end_call_main);
            endRelativeLayout.setBackgroundColor(Color.parseColor(themeConfig.getSecondaryMainColor()));
            TextView txtWarningEndCall = dialogView.findViewById(R.id.txtWarningEndCall);
            txtWarningEndCall.setTextColor(Color.parseColor(themeConfig.getSecondaryContrastColor()));
            txtWarningEndCall.setText("Are You Sure?\nYou will loose your progress on going back");
            builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    try {
                        if (msAdaptor != null) {
                            Log.d("==Hang", "Close all: back button pressed");
                            msAdaptor.destroyJanus();
                            msAdaptor.hangUp();
                            msAdaptor.disconnectRoomIDSocket();
                        }
                        JSONObject object = new JSONObject();
                        object.put("event","USER_BACKPRESS");
                        videoComponentCallBack.onVCIntermediate(object);


                        //  callEndedLayout.setVisibility(View.GONE);
                       // sendEndCallTOBaseFragment();
                    } catch (Exception e) {
                        System.out.println("Force close: " + e);
                    }
                    dialog.dismiss();
                }
            });
            builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            builder.setView(dialogView);
            alertEndCallDialog = builder.create();
            alertEndCallDialog.setCanceledOnTouchOutside(false);
        }
        if (!alertEndCallDialog.isShowing()) {
            alertEndCallDialog.show();
        }
    }

    private void btnCallControl(int isVisible) {
        fabCameraSwitch.setVisibility(isVisible);
        fabHamburger.setVisibility(isVisible);
        fabEndCall.setVisibility(isVisible);
        rlControl.setVisibility(isVisible);

    }

    private void initView(View view) {
        hideToolbar();
        populateNetworkSignalList();
        rcViewNetwork = view.findViewById(R.id.rc_view_network);
        rcViewNetwork.setHasFixedSize(true);
        rcViewNetwork.setLayoutManager(new LinearLayoutManager(requireActivity()));
        networkSignalAdapter = new NetworkSignalAdapter(networkSignalModelList);
        rcViewNetwork.setAdapter(networkSignalAdapter);
        // infoMainLayout = view.findViewById(R.id.info_main_layout);
        fab_hamburger_open = AnimationUtils.loadAnimation(getActivity(), R.anim.fab_open);
        fab_hamburger_close = AnimationUtils.loadAnimation(getActivity(), R.anim.fab_close);
        cameraRootLayout = view.findViewById(R.id.camera_root);
        fabHamburger = view.findViewById(R.id.fab_hamburger);
        fabHamburger.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                animateHamburger();
            }
        });
        fabCameraSwitch = view.findViewById(R.id.fab_camera_switch);
        fabCameraSwitch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 3000){
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();
                int facing = getFacing();
                setFacing(facing == CameraConstants.FACING_FRONT ?
                        CameraConstants.FACING_BACK : CameraConstants.FACING_FRONT);
            }
        });
        fabMic = view.findViewById(R.id.fab_mic);
        fabMic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //boolean isMicMuted = msAdapter.muteUnMute();
                if (msAdaptor != null) {
                    isMicMuted = msAdaptor.isAudioEnabled();
                    if (isMicMuted) {
                        msAdaptor.setAudio(false);
                        fabMic.setSelected(true);
                    } else {
                        msAdaptor.setAudio(true);
                        fabMic.setSelected(false);
                    }
                }
            }
        });
        fabVideoSwitch = view.findViewById(R.id.fab_video);
        fabVideoSwitch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isCameraOpened()) {
                    stop();
                    if (msAdaptor != null) {
                        msAdaptor.setVideoEnable(false);
                    }
                    mTextureView.setVisibility(View.GONE);
                    fabVideoSwitch.setSelected(true);
                } else {
                    if (msAdaptor != null) {
                        msAdaptor.setVideoEnable(true);
                    }
                    mTextureView.setVisibility(View.VISIBLE);
                    start();
                    fabVideoSwitch.setSelected(false);
                }
            }
        });
        fabEndCall = view.findViewById(R.id.fab_end_call);
        fabEndCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showEndCallDialog();
            }
        });
        rlControl = view.findViewById(R.id.rlControl);
        connectionIndicator = view.findViewById(R.id.connectionIndicator);
        connectionIndicator.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showHideNetworkStats();

            }
        });
        txtCantSee = view.findViewById(R.id.txt_cant_see);
        txtCantSee.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendNoAudioNoVideo("no_video");

            }
        });
        txtCantHear = view.findViewById(R.id.txt_cant_hear);
        txtCantHear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendNoAudioNoVideo("no_audio");

            }
        });
        audioManager = (AudioManager) getActivity().getSystemService(Context.AUDIO_SERVICE);
        audioManager.setMode(AudioManager.MODE_IN_CALL);
        isSpeaker = false;
        audioManager.setSpeakerphoneOn(isSpeaker);
        mTextureView = (AutoFitTextureView) view.findViewById(R.id.texture);
        mFocusView = (FocusView) view.findViewById(R.id.focusView);
        mTextureView.setGestureListener(new GestureDetector.SimpleOnGestureListener() {
            @Override
            public boolean onSingleTapUp(MotionEvent e) {
                setFocusViewWidthAnimation((int) e.getX(), (int) e.getY());
                setManualFocusAt((int) e.getX(), (int) e.getY());
                return true;
            }
        });

        mTextureView.setScaleGesture(new ScaleGestureDetector.SimpleOnScaleGestureListener() {
            @Override
            public boolean onScale(ScaleGestureDetector detector) {
                updateZoom(zoom * detector.getScaleFactor());
                updatePreview();
                return true;
            }
        });
//        btnSpeaker = view.findViewById(R.id.btnSpeaker);
//        btnSpeaker.setSelected(true);
//        btnSpeaker.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                isSpeaker = !isSpeaker;
//                btnSpeaker.setSelected(isSpeaker);
//                audioManager.setSpeakerphoneOn(isSpeaker);
//            }
//        });
//        spiBitRate = view.findViewById(R.id.spiBitRate);
//        spiBitRate.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                String bitRate = parent.getSelectedItem().toString();
//                if (!isFirstRun){
//                    msAdapter.bitrate(Integer.valueOf(bitRate));
//                }
//                isFirstRun = false;
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//            }
//        });
//        btnSpeaker = view.findViewById(R.id.btnSpeaker);
//        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(),
//                R.array.bitrate_array, android.R.layout.simple_spinner_item);
//        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        spiBitRate.setAdapter(adapter);
        mFocusView = (FocusView) view.findViewById(R.id.focusView);
        remoteViewRenderer = view.findViewById(R.id.remote_view);
        remoteViewRenderer.setMirror(true);
        remoteViewRenderer.setEnableHardwareScaler(true);
        remoteViewRenderer.init(eglBase.getEglBaseContext(), null);
        remoteViewRenderer.setVisibility(View.GONE);

    }


    public void animateHamburger() {
        if (isHamBurgerOpen) {
            fabMic.setVisibility(View.INVISIBLE);
            fabVideoSwitch.setVisibility(View.INVISIBLE);
            fabMic.startAnimation(fab_hamburger_close);
            fabVideoSwitch.startAnimation(fab_hamburger_close);
            fabMic.setClickable(false);
            fabVideoSwitch.setClickable(false);
            isHamBurgerOpen = false;
        } else {
            fabMic.setVisibility(View.VISIBLE);
            fabVideoSwitch.setVisibility(View.VISIBLE);
            fabMic.startAnimation(fab_hamburger_open);
            fabVideoSwitch.startAnimation(fab_hamburger_open);
            fabMic.setClickable(true);
            fabVideoSwitch.setClickable(true);
            isHamBurgerOpen = true;
        }
    }

    private void populateNetworkSignalList() {
        networkSignalModelList = new ArrayList<>();
        networkSignalModelList.add(new NetworkSignalModel("Stats", "User Local", "User Remote"));
        networkSignalModelList.add(new NetworkSignalModel("Average Incoming Bitrate (kbps)", "0", "0"));
        networkSignalModelList.add(new NetworkSignalModel("Average Outgoing Bitrate (kbps)", "0", "0"));
        networkSignalModelList.add(new NetworkSignalModel("Current Incoming Bitrate (kbps)", "0", "0"));
        networkSignalModelList.add(new NetworkSignalModel("Current Outgoing Bitrate (kbps)", "0", "0"));
        networkSignalModelList.add(new NetworkSignalModel("Current Time", "0", "0"));
        networkSignalModelList.add(new NetworkSignalModel("Current Bytes Sent", "0", "0"));
        networkSignalModelList.add(new NetworkSignalModel("Current Bytes Received", "0", "0"));
        networkSignalModelList.add(new NetworkSignalModel("Packets Lost", "0", "0"));
        networkSignalModelList.add(new NetworkSignalModel("Fraction Lost", "0", "0"));
    }

    private void showHideNetworkStats() {
        if (rcViewNetwork.getVisibility() == View.GONE) {
            toggle(true);
        } else {
            toggle(false);

        }
    }

    private void toggle(boolean show) {
        View redLayout = rcViewNetwork;
        Transition transition = new Slide(Gravity.TOP);
        transition.setDuration(600);
        transition.addTarget(redLayout);

        TransitionManager.beginDelayedTransition(cameraRootLayout, transition);
        redLayout.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    private void showEndCallDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        ViewGroup viewGroup = view.findViewById(android.R.id.content);
        View dialogView = LayoutInflater.from(getActivity()).inflate(R.layout.end_call_dialog_layout, viewGroup, false);
        RelativeLayout endRelativeLayout = dialogView.findViewById(R.id.warning_end_call_main);
        endRelativeLayout.setBackgroundColor(Color.parseColor(themeConfig.getSecondaryMainColor()));
        TextView txtWarningEndCall = dialogView.findViewById(R.id.txtWarningEndCall);
        txtWarningEndCall.setTextColor(Color.parseColor(themeConfig.getSecondaryContrastColor()));
        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
                executor.execute(new Runnable() {
                    @Override
                    public void run() {
                        if (msAdaptor != null) {
                            msAdaptor.destroyJanus();
                            msAdaptor.hangUp();
                            msAdaptor.disconnectRoomIDSocket();

                        }
                    }
                });
                //renderCallEndedUi();
                JSONObject toolbarObject = new JSONObject();
                try {
                    toolbarObject.put("event", "USER_END_CALL");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                videoComponentCallBack.onVCIntermediate(toolbarObject);
            }
        });
        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();

            }
        });
        builder.setView(dialogView);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void hideToolbar() {
        JSONObject toolbarObject = new JSONObject();
        try {
            toolbarObject.put("event", "TOOLBAR");
            toolbarObject.put("visibility", false);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        videoComponentCallBack.onVCIntermediate(toolbarObject);
    }

    public void networkDisconnectonOnSessionError() {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                if (msAdaptor != null) {
                    Log.d("==Hang", "HANG");
                    msAdaptor.destroyJanus();
                    msAdaptor.hangUp();
                    msAdaptor.disconnectRoomIDSocket();
                    msAdaptor.disconnectOdSocket();
                }
            }
        });
    }

    private void sendReConnectingEvent(String reason) {
        JSONObject reconnectObject = new JSONObject();
        try {
            reconnectObject.put("ms_room_id", reconnectingId);
            reconnectObject.put("type", "auto");
            reconnectObject.put("reason", "ICEConnectionState " + reason);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        msAdaptor.getRoomSocket().sendEvent("reconnect", reconnectObject,
                new ApiCallBack() {
                    @Override
                    public void onSessionSuccess(JSONObject object) {
                    }

                    @Override
                    public void intermediateCallBack() {

                    }

                    @Override
                    public void onSessionFailure(String message) {
                        Log.d("===RECONNECT", "MESS " + message);

                    }
                });
    }

    public void showConnectingDialog(String purpose) {
        if (alertDialog == null) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            ViewGroup viewGroup = view.findViewById(android.R.id.content);
            View dialogView = LayoutInflater.from(getActivity()).inflate(R.layout.reconnecting_dialog_layout, viewGroup, false);
            TextView title = dialogView.findViewById(R.id.title);
            TextView content = dialogView.findViewById(R.id.content);
            if (purpose.equals("connecting")) {
                title.setText(R.string.connecting);
                content.setText(R.string.connecting_to_the_call);
            }
            builder.setCancelable(false);
            builder.setView(dialogView);
            alertDialog = builder.create();
        }
        if (!alertDialog.isShowing()) {
            alertDialog.show();
        }


    }


    @Override
    public void setRoomCall(JSONObject roomCallObject) {
        Log.d("===SETROOM", "RECONNECTING COMPLETED" + roomCallObject.toString());
        try {
            streamReferenceId = "";
            agentStreamId = "";
            JSONObject instanceObject = roomCallObject.getJSONObject("instance");
            JSONObject peerObject = instanceObject.getJSONObject("config").getJSONObject("peerconnection_config");
            JSONArray iceServerArray = peerObject.getJSONArray("iceServers");
            hostIp = instanceObject.getString("link");
            Log.d("===SETROOM", "RECONNECTING COMPLETED" + hostIp);


            //  hostIp = "wss://ms.idfystaging.com/media-server/j1/socketxyz";
            String providerName = instanceObject.getString("provider_name");
            reconnectingId = roomCallObject.getInt("ms_room_id");
            roomId = roomCallObject.getString("ms_room_reference_id");
            JSONArray participantsArray = roomCallObject.getJSONArray("participants");
            for (int i = 0; i < participantsArray.length(); i++) {
                JSONObject participantObject = participantsArray.getJSONObject(i);
                String id = participantObject.getString("participant_id");
                System.out.println("===STREAM" + participantId + "  id: " + id);
                JSONObject dataObject = participantObject.getJSONObject("data");
                if (id.equals(participantId)) {
                    streamReferenceId = dataObject.getString("stream_reference_id");
                } else {
                    agentStreamId = dataObject.getString("stream_reference_id");
                }
            }
            new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                @Override
                public void run() {
                    startJanus(hostIp, roomId, iceServerArray, providerName);

                }
            }, 3000);
        } catch (Exception e) {
            System.out.println("===>" + e);
        }
    }

    private void startJanus(String hostIp, String roomId, JSONArray iceServerArray, String providerName) {
        if (msAdaptor != null) {
            msAdaptor.initServer(providerName, hostIp, roomId, reconnectingId, streamReferenceId, agentStreamId,
                    requireActivity(), eglBase.getEglBaseContext(), mTextureView, remoteViewRenderer, iceServerArray,
                    dalCapture.isVideoOverlayEnabled(), dalCapture.getLatitude(), dalCapture.getLongitude(),
                    new MsAdaptorCallBack() {
                        @Override
                        public void onPublishSuccess(boolean isPublishSuccess, boolean isRemoteStreamReceived, String iceState) {
                            if (isPublishSuccess && !isRemoteStreamReceived && iceState.equals("connected")) {
                                isReconnecting = false;
                                if (msAdaptor != null) {
                                    msAdaptor.hangUp();
                                    msAdaptor.disconnectRoomIDSocket();
                                }
                                JSONObject healthCheckObject = new JSONObject();
                                try {
                                    healthCheckObject.put("event", "HEALTH_CHECK");
                                    healthCheckObject.put("status", true);
                                    healthCheckObject.put("ice_state", iceState);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                videoComponentCallBack.onVCSuccess(healthCheckObject);
                                //   submitHealthCheck(networkID, true, iceState);

                            }
                            if (isPublishSuccess && isRemoteStreamReceived && iceState.equals("connected")) {
                                isReconnecting = false;
                                requireActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (!isCameraOpened()) {
                                            start();
                                            mTextureView.setVisibility(View.VISIBLE);
                                            fabVideoSwitch.setSelected(false);
                                        }
                                        if (isMicMuted) {
                                            fabMic.setSelected(false);
                                        }
                                        if (isHealthChecked) {
                                            waitingRoomLayout.setVisibility(View.INVISIBLE);
                                        }
                                        hideConnectingDialog();
                                        btnCallControl(View.VISIBLE);
                                        remoteViewRenderer.setVisibility(View.VISIBLE);
                                        cameraRootLayout.setVisibility(View.VISIBLE);
                                        //  callEndedLayout.setVisibility(View.GONE);
                                    }
                                });
                            }

                        }

                        @Override
                        public void onIceSateFailed(String state) {
                            if (!isReconnecting) {
                                sendReConnectingEvent(state);
                            }
                        }

                        @Override
                        public void onNetworkSignalStats(NetworkSignalModel signalModel) {
                            requireActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    String ragStatus = "";
                                    connectionIndicator.setVisibility(View.VISIBLE);
                                    if (signalModel.getAverageIncomingBitrate() <= AVERAGE_NETWORK_THRESHOLD &&
                                            signalModel.getAverageOutgoingBitrate() <= AVERAGE_NETWORK_THRESHOLD) {
                                        connectionIndicator.startSearching();
                                        ragStatus = "red";
                                    } else if ((signalModel.getAverageIncomingBitrate() > AVERAGE_NETWORK_THRESHOLD &&
                                            signalModel.getAverageIncomingBitrate() <= STRONG_NETWORK_THRESHOLD) ||
                                            (signalModel.getAverageOutgoingBitrate() > AVERAGE_NETWORK_THRESHOLD &&
                                                    signalModel.getAverageOutgoingBitrate() <= STRONG_NETWORK_THRESHOLD)) {
                                        connectionIndicator.stopSearching();
                                        connectionIndicator.displayConnectionLevel(3);
                                        ragStatus = "amber";
                                    } else if (signalModel.getAverageIncomingBitrate() > STRONG_NETWORK_THRESHOLD ||
                                            signalModel.getAverageOutgoingBitrate() > STRONG_NETWORK_THRESHOLD) {
                                        connectionIndicator.stopSearching();
                                        connectionIndicator.displayConnectionLevel(5);
                                        ragStatus = "green";
                                    }
                                    signalModel.setRagStatus(ragStatus);
                                    signalModel.setToken(token);
                                    signalModel.setStreamType((signalModel.isPublish() ? "user" : "operator"));
                                    Date date = new Date(signalModel.getCurrentTimestamp());
                                    SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss", Locale.US);
                                    if (signalModel.isPublish()) {
                                        networkSignalModelList.get(1).setLabel2(String.valueOf(signalModel.getAverageIncomingBitrate()));
                                        networkSignalModelList.get(2).setLabel2(String.valueOf(signalModel.getAverageOutgoingBitrate()));
                                        networkSignalModelList.get(3).setLabel2(String.valueOf(signalModel.getCurrentIncomingBitrate()));
                                        networkSignalModelList.get(4).setLabel2(String.valueOf(signalModel.getCurrentOutgoingBitrate()));
                                        networkSignalModelList.get(5).setLabel2(sdf.format(date));
                                        networkSignalModelList.get(8).setLabel2(String.valueOf(signalModel.getPacketsLost()));
                                    } else {
                                        networkSignalModelList.get(1).setLabel3(String.valueOf(signalModel.getAverageIncomingBitrate()));
                                        networkSignalModelList.get(2).setLabel3(String.valueOf(signalModel.getAverageOutgoingBitrate()));
                                        networkSignalModelList.get(3).setLabel3(String.valueOf(signalModel.getCurrentIncomingBitrate()));
                                        networkSignalModelList.get(4).setLabel3(String.valueOf(signalModel.getCurrentOutgoingBitrate()));
                                        networkSignalModelList.get(5).setLabel3(sdf.format(date));
                                        networkSignalModelList.get(8).setLabel3(String.valueOf(signalModel.getPacketsLost()));
                                    }
                                    networkSignalAdapter.notifyDataSetChanged();
                                    JSONObject object = new JSONObject();
                                    try {
                                        object.put("event","WEBRTC_STATS");
                                        object.put("data",new JSONObject(new Gson().toJson(signalModel)));
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    videoComponentCallBack.onVCIntermediate(object);
//                                    try {
//                                        msAdaptor.getODSocket().sendEvent("webrtc_stats",new JSONObject(new Gson().toJson(signalModel)));
//                                    } catch (JSONException e) {
//                                        e.printStackTrace();
//                                    }

                                }
                            });
                        }
                    });
        }
    }

    @Override
    public void onReconnecting() {
        Log.d("===RECONNECT", "RECONNECTING");
        isReconnecting = true;
        if (msAdaptor != null) {
            //msAdapter.destroyJanus();
            msAdaptor.hangUp();
        }
        requireActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                showConnectingDialog("reconnecting");
            }
        });

    }

    @Override
    public void onSocketReconnecting() {
        requireActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                showConnectingDialog("reconnecting");
            }
        });
    }


    @Override
    public void onDisconnect(String code) {
        if (code.equals("reconnects_exhausted")) {
            if (msAdaptor != null) {
                msAdaptor.destroyJanus();
                msAdaptor.hangUp();
                msAdaptor.disconnectRoomIDSocket();
            }

            requireActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    hideConnectingDialog();
                    JSONObject object = new JSONObject();
                    try {
                        object.put("event", "RECONNECTS_EXHAUSTED");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    videoComponentCallBack.onVCFailure(object);
                }
            });
        } else if (code.equals("server_ended_room") || code.equals("left_room")) {
            requireActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    hideConnectingDialog();
                }
            });

        } else {
            if (msAdaptor != null) {
                msAdaptor.destroyJanus();
                msAdaptor.hangUp();
                msAdaptor.disconnectRoomIDSocket();
            }
            String[] codeStrings = code.split(" ");
            JSONObject object = new JSONObject();
            if (this.participantId.equals(codeStrings[1])) {
                try {
                    object.put("event", "USER_EXIT");
                    object.put("reason", codeStrings[0]);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                try {
                    object.put("event", "OPERATOR_EXIT");
                    object.put("reason", codeStrings[0]);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            videoComponentCallBack.onVCFailure(object);
            requireActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    hideConnectingDialog();
                }
            });
        }
    }


    public void cleanUp() {
        try {
            if (msAdaptor != null) {
                msAdaptor.destroyJanus();
                msAdaptor.hangUp();
                msAdaptor.disconnectRoomIDSocket();
                //  msAdaptor.sendEndCallEvent("agent_ended_call", "ended", false);
                // msAdaptor.disconnectOdSocket();
            }
            requireActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    hideConnectingDialog();
                    dalCapture.setCalCompleted(true);
                }
            });

        } catch (Exception e) {
        }

    }

    public void sendNoAudioNoVideo(String event) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("event", event.toUpperCase(Locale.ROOT));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        videoComponentCallBack.onVCIntermediate(jsonObject);
    }

    private void startScreenCapture() {
//        MediaProjectionManager mediaProjectionManager =
//                (MediaProjectionManager) getApplication().getSystemService(
//                        Context.MEDIA_PROJECTION_SERVICE);
//        startActivityForResult(
//                mediaProjectionManager.createScreenCaptureIntent(), 1);

        MediaProjectionManager manager = (MediaProjectionManager) getActivity().getSystemService(MEDIA_PROJECTION_SERVICE);
        if (manager == null) {
            //showShortToast ( "screenshots service unavailable");
            Toast.makeText(getActivity(), "screenshots service unavailable", Toast.LENGTH_SHORT).show();
            return;
        }
        Intent intent = manager.createScreenCaptureIntent();
        startActivityForResult(intent, 1);


    }

    private void hideConnectingDialog() {
        if (alertDialog != null) {
            if (alertDialog.isShowing()) {
                alertDialog.dismiss();
                alertDialog = null;
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode != 1)
            return;
        mMediaProjectionPermissionResultCode = resultCode;
        mMediaProjectionPermissionResultData = data;
        isScreenShared = true;
        capturer = createScreenCapturer();
        // msAdapter.shareScreen(capturer);
       /* MyInit myInit = new MyInit(hostIp,roomId);
        myInit.run();*/
    }

    @TargetApi(21)
    private VideoCapturer createScreenCapturer() {
        return new ScreenCapturerAndroid(
                mMediaProjectionPermissionResultData, new MediaProjection.Callback() {
            @Override
            public void onStop() {
                Log.d("===TAG", "User didn't give permission to capture the screen.");
            }
        });
    }


    @Override
    public void onResume() {
        super.onResume();
        start();
    }

    @Override
    public void onPause() {
        stop();
        super.onPause();
    }

    public void start() {
        startBackgroundThread();

        // When the screen is turned off and turned back on, the SurfaceTexture is already
        // available, and "onSurfaceTextureAvailable" will not be called. In that case, we can open
        // a camera and start preview from here (otherwise, we wait until the surface is ready in
        // the SurfaceTextureListener).
        if (mTextureView.isAvailable()) {
            openCamera(mTextureView.getWidth(), mTextureView.getHeight());
        } else {
            mTextureView.setSurfaceTextureListener(mSurfaceTextureListener);
        }
    }

    public void stop() {
        closeCamera();
        stopBackgroundThread();
    }

    private float getFingerSpacing(MotionEvent event) {
        float x = event.getX(0) - event.getX(1);
        float y = event.getY(0) - event.getY(1);
        return (float) Math.sqrt(x * x + y * y);
    }

    /**
     * Focus view animation
     */
    private void setFocusViewWidthAnimation(float x, float y) {
        mFocusView.setVisibility(View.VISIBLE);

        mFocusView.setX(x - mFocusView.getWidth() / 2);
        mFocusView.setY(y - mFocusView.getHeight() / 2);

        ObjectAnimator scaleX = ObjectAnimator.ofFloat(mFocusView, "scaleX", 1, 0.6f);
        ObjectAnimator scaleY = ObjectAnimator.ofFloat(mFocusView, "scaleY", 1, 0.6f);
        ObjectAnimator alpha = ObjectAnimator.ofFloat(mFocusView, "alpha", 1f, 0.3f, 1f, 0.3f, 1f, 0.3f, 1f);
        AnimatorSet animSet = new AnimatorSet();
        animSet.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mFocusView.setVisibility(View.INVISIBLE);
            }
        });
        animSet.play(scaleX).with(scaleY).before(alpha);
        animSet.setDuration(300);
        animSet.start();
    }

    /**
     * Setup member variables related to camera.
     *
     * @param width  The width of available size for camera preview
     * @param height The height of available size for camera preview
     */
    private void setupCameraOutputs(int width, int height) {
        Activity activity = getActivity();
        int internalFacing = INTERNAL_FACINGS.get(mFacing);
        CameraManager manager = (CameraManager) activity.getSystemService(Context.CAMERA_SERVICE);
        try {
            String[] cameraIds = manager.getCameraIdList();
            mFacingSupported = cameraIds.length > 1;
            for (String cameraId : cameraIds) {
                mCameraCharacteristics
                        = manager.getCameraCharacteristics(cameraId);

                Integer facing = mCameraCharacteristics.get(CameraCharacteristics.LENS_FACING);
                if (facing == null || facing != internalFacing) {
                    continue;
                }

                StreamConfigurationMap map = mCameraCharacteristics.get(
                        CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);
                if (map == null) {
                    continue;
                }
                Float maxZoomObject = mCameraCharacteristics.get(CameraCharacteristics.SCALER_AVAILABLE_MAX_DIGITAL_ZOOM);
                if (maxZoomObject != null) {
                    maxZoom = maxZoomObject;
                }

                // Find out if we need to swap dimension to get the preview size relative to sensor
                // coordinate.
                int displayRotation = activity.getWindowManager().getDefaultDisplay().getRotation();
                //noinspection ConstantConditions
                mSensorOrientation = mCameraCharacteristics.get(CameraCharacteristics.SENSOR_ORIENTATION);
                boolean swappedDimensions = false;
                switch (displayRotation) {
                    case Surface.ROTATION_0:
                    case Surface.ROTATION_180:
                        if (mSensorOrientation == 90 || mSensorOrientation == 270) {
                            swappedDimensions = true;
                        }
                        break;
                    case Surface.ROTATION_90:
                    case Surface.ROTATION_270:
                        if (mSensorOrientation == 0 || mSensorOrientation == 180) {
                            swappedDimensions = true;
                        }
                        break;
                    default:
                        Log.e(TAG, "Display rotation is invalid: " + displayRotation);
                }

                Point displaySize = new Point();
                activity.getWindowManager().getDefaultDisplay().getRealSize(displaySize);
                int rotatedPreviewWidth = width;
                int rotatedPreviewHeight = height;
                int maxPreviewWidth = displaySize.x;
                int maxPreviewHeight = displaySize.y;

                if (swappedDimensions) {
                    rotatedPreviewWidth = height;
                    rotatedPreviewHeight = width;
                    maxPreviewWidth = displaySize.y;
                    maxPreviewHeight = displaySize.x;
                }

                if (maxPreviewWidth > MAX_PREVIEW_WIDTH) {
                    maxPreviewWidth = MAX_PREVIEW_WIDTH;
                }

                if (maxPreviewHeight > MAX_PREVIEW_HEIGHT) {
                    maxPreviewHeight = MAX_PREVIEW_HEIGHT;
                }

                // Danger, W.R.! Attempting to use too large a preview size could exceed the camera
                // bus' bandwidth limitation, resulting in gorgeous previews but the storage of
                // garbage capture data.
                mPreviewSize = chooseOptimalSize(map.getOutputSizes(SurfaceTexture.class),
                        rotatedPreviewWidth, rotatedPreviewHeight, maxPreviewWidth,
                        maxPreviewHeight);

                mVideoSize = chooseVideoSize(map.getOutputSizes(MediaRecorder.class));

                // For still image captures, we use the largest available size.
                Size largest = choosePictureSize(map.getOutputSizes(ImageFormat.JPEG));

                mImageReader = ImageReader.newInstance(largest.getWidth(), largest.getHeight(),
                        ImageFormat.JPEG, /*maxImages*/2);
                mImageReader.setOnImageAvailableListener(
                        mOnImageAvailableListener, mBackgroundHandler);

                // We fit the aspect ratio of TextureView to the size of preview we picked.
                int orientation = getResources().getConfiguration().orientation;
                if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
                    mTextureView.setAspectRatio(mPreviewSize.getWidth(), mPreviewSize.getHeight());
                } else {
                    mTextureView.setAspectRatio(mPreviewSize.getHeight(), mPreviewSize.getWidth());
                }

                checkAutoFocusSupported();
                checkFlashSupported();

                mCropRegion = AutoFocusHelper.cropRegionForZoom(mCameraCharacteristics,
                        CameraConstants.ZOOM_REGION_DEFAULT);

                mCameraId = cameraId;
                Log.i(TAG, "CameraId: " + mCameraId + " ,isFlashSupported: " + mFlashSupported);
                return;
            }
        } catch (CameraAccessException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            // Currently an NPE is thrown when the Camera2API is used but not supported on the
            // device this code runs.
//            ErrorDialog.newInstance(getString(R.string.camera_error))
//                    .show(getChildFragmentManager(), FRAGMENT_DIALOG);
        }
    }

    /**
     * Check if the auto focus is supported.
     */
    private void checkAutoFocusSupported() {
        int[] modes = mCameraCharacteristics.get(CameraCharacteristics.CONTROL_AF_AVAILABLE_MODES);
        mAutoFocusSupported = !(modes == null || modes.length == 0 ||
                (modes.length == 1 && modes[0] == CameraCharacteristics.CONTROL_AF_MODE_OFF));
    }

    /**
     * Check if the flash is supported.
     */
    private void checkFlashSupported() {
        Boolean available = mCameraCharacteristics.get(CameraCharacteristics.FLASH_INFO_AVAILABLE);
        mFlashSupported = available == null ? false : available;
    }

    private void openCamera(int width, int height) {
        setupCameraOutputs(width, height);
        configureTransform(width, height);
        Activity activity = getActivity();
        CameraManager manager = (CameraManager) activity.getSystemService(Context.CAMERA_SERVICE);
        try {
            if (!mCameraOpenCloseLock.tryAcquire(CameraConstants.OPEN_CAMERA_TIMEOUT_MS, TimeUnit.MILLISECONDS)) {
                throw new RuntimeException("Time out waiting to lock camera opening.");
            }
            mMediaRecorder = new MediaRecorder();
            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            manager.openCamera(mCameraId, mStateCallback, mBackgroundHandler);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            throw new RuntimeException("Interrupted while trying to lock camera opening.", e);
        }
        fabCameraSwitch.setEnabled(true);

    }

    /**
     * Closes the current {@link CameraDevice}.
     */
    private void closeCamera() {
        try {
            mCameraOpenCloseLock.acquire();
            if (null != mPreviewSession) {
                mPreviewSession.close();
                mPreviewSession = null;
            }
            if (null != mCameraDevice) {
                mCameraDevice.close();
                mCameraDevice = null;
            }
            if (null != mMediaRecorder) {
                mMediaRecorder.release();
                mMediaRecorder = null;
            }
            if (null != mImageReader) {
                mImageReader.close();
                mImageReader = null;
            }
        } catch (InterruptedException e) {
            throw new RuntimeException("Interrupted while trying to lock camera closing.", e);
        } finally {
            mCameraOpenCloseLock.release();
        }
    }

    /**
     * Starts a background thread and its {@link Handler}.
     */
    private void startBackgroundThread() {
        mBackgroundThread = new HandlerThread("CameraBackground");
        mBackgroundThread.start();
        mBackgroundHandler = new Handler(mBackgroundThread.getLooper()) {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                switch (msg.what) {
                    case MSG_CAPTURE_PICTURE_WHEN_FOCUS_TIMEOUT:
                        mState = STATE_PICTURE_TAKEN;
                        captureStillPicture();
                        break;
                    default:
                        break;
                }

            }
        };
    }

    /**
     * Stops the background thread and its {@link Handler}.
     */
    private void stopBackgroundThread() {
        try {
            mBackgroundThread.quitSafely();
            mBackgroundThread.join();
            mBackgroundThread = null;
            mBackgroundHandler = null;
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Creates a new {@link CameraCaptureSession} for camera preview.
     */
    private void createCameraPreviewSession() {
        try {
            SurfaceTexture texture = mTextureView.getSurfaceTexture();
            assert texture != null;

            // We configure the size of default buffer to be the size of camera preview we want.
            texture.setDefaultBufferSize(mPreviewSize.getWidth(), mPreviewSize.getHeight());

            // This is the output Surface we need to start preview.
            Surface surface = new Surface(texture);

            // We set up a CaptureRequest.Builder with the output Surface.
            mPreviewRequestBuilder = mCameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW);
            mPreviewRequestBuilder.addTarget(surface);

            // Here, we create a CameraCaptureSession for camera preview.
            mCameraDevice.createCaptureSession(Arrays.asList(surface, mImageReader.getSurface()),
                    new CameraCaptureSession.StateCallback() {

                        @Override
                        public void onConfigured(@NonNull CameraCaptureSession cameraCaptureSession) {
                            // The camera is already closed
                            if (null == mCameraDevice) {
                                return;
                            }

                            // When the session is ready, we start displaying the preview.
                            mPreviewSession = cameraCaptureSession;
                            try {
                                // Auto focus should be continuous for camera preview.
//                            mPreviewRequestBuilder.set(CaptureRequest.CONTROL_AF_MODE,
//                                CaptureRequest.CONTROL_AF_MODE_CONTINUOUS_PICTURE);
                                updateAutoFocus();
                                // Flash is automatically enabled when necessary.
                                updateFlash(mPreviewRequestBuilder);

                                // Finally, we start displaying the camera preview.
                                mPreviewRequest = mPreviewRequestBuilder.build();
                                mPreviewSession.setRepeatingRequest(mPreviewRequest,
                                        mCaptureCallback, mBackgroundHandler);
                            } catch (CameraAccessException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onConfigureFailed(
                                @NonNull CameraCaptureSession cameraCaptureSession) {
                            showToast("Create preview configure failed");
                        }
                    }, mBackgroundHandler);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    /**
     * Configures the necessary {@link Matrix} transformation to `mTextureView`.
     * This method should be called after the camera preview size is determined in
     * setupCameraOutputs and also the size of `mTextureView` is fixed.
     *
     * @param viewWidth  The width of `mTextureView`
     * @param viewHeight The height of `mTextureView`
     */
    private void configureTransform(int viewWidth, int viewHeight) {
        Activity activity = getActivity();
        if (null == mTextureView || null == mPreviewSize || null == activity) {
            return;
        }
        int rotation = activity.getWindowManager().getDefaultDisplay().getRotation();
        Matrix matrix = new Matrix();
        RectF viewRect = new RectF(0, 0, viewWidth, viewHeight);
        RectF bufferRect = new RectF(0, 0, mPreviewSize.getHeight(), mPreviewSize.getWidth());
        float centerX = viewRect.centerX();
        float centerY = viewRect.centerY();
        if (Surface.ROTATION_90 == rotation || Surface.ROTATION_270 == rotation) {
            bufferRect.offset(centerX - bufferRect.centerX(), centerY - bufferRect.centerY());
            matrix.setRectToRect(viewRect, bufferRect, Matrix.ScaleToFit.FILL);
            float scale = Math.max(
                    (float) viewHeight / mPreviewSize.getHeight(),
                    (float) viewWidth / mPreviewSize.getWidth());
            matrix.postScale(scale, scale, centerX, centerY);
            matrix.postRotate(90 * (rotation - 2), centerX, centerY);
        } else if (Surface.ROTATION_180 == rotation) {
            matrix.postRotate(180, centerX, centerY);
        }
        mTextureView.setTransform(matrix);
    }

    public boolean isCameraOpened() {
        return mCameraDevice != null;
    }

    public void setFacing(int facing) {
        if (mFacing == facing) {
            return;
        }
        mFacing = facing;
        if (isCameraOpened()) {
            stop();
            start();
        }
    }

    public int getFacing() {
        return mFacing;
    }

    /**
     * Updates the internal state of flash to {@link #mFlash}.
     */
    void updateFlash(CaptureRequest.Builder requestBuilder) {
        if (!mFlashSupported) {
            return;
        }
        switch (mFlash) {
            case CameraConstants.FLASH_OFF:
                requestBuilder.set(CaptureRequest.CONTROL_AE_MODE,
                        CaptureRequest.CONTROL_AE_MODE_ON);
                requestBuilder.set(CaptureRequest.FLASH_MODE,
                        CaptureRequest.FLASH_MODE_OFF);
                break;
            case CameraConstants.FLASH_ON:
                requestBuilder.set(CaptureRequest.CONTROL_AE_MODE,
                        CaptureRequest.CONTROL_AE_MODE_ON_ALWAYS_FLASH);
                requestBuilder.set(CaptureRequest.FLASH_MODE,
                        CaptureRequest.FLASH_MODE_OFF);
                break;
            case CameraConstants.FLASH_TORCH:
                requestBuilder.set(CaptureRequest.CONTROL_AE_MODE,
                        CaptureRequest.CONTROL_AE_MODE_ON);
                requestBuilder.set(CaptureRequest.FLASH_MODE,
                        CaptureRequest.FLASH_MODE_TORCH);
                break;
            case CameraConstants.FLASH_AUTO:
                requestBuilder.set(CaptureRequest.CONTROL_AE_MODE,
                        CaptureRequest.CONTROL_AE_MODE_ON_AUTO_FLASH);
                requestBuilder.set(CaptureRequest.FLASH_MODE,
                        CaptureRequest.FLASH_MODE_OFF);
                break;
            case CameraConstants.FLASH_RED_EYE:
                requestBuilder.set(CaptureRequest.CONTROL_AE_MODE,
                        CaptureRequest.CONTROL_AE_MODE_ON_AUTO_FLASH_REDEYE);
                requestBuilder.set(CaptureRequest.FLASH_MODE,
                        CaptureRequest.FLASH_MODE_OFF);
                break;
        }
    }


    /**
     * Updates the internal state of auto-focus to {@link #mAutoFocus}.
     */
    void updateAutoFocus() {
        if (mAutoFocus) {
            if (!mAutoFocusSupported) {
                mPreviewRequestBuilder.set(CaptureRequest.CONTROL_AF_MODE,
                        CaptureRequest.CONTROL_AF_MODE_OFF);
            } else {
                if (mIsRecordingVideo) {
                    mPreviewRequestBuilder.set(CaptureRequest.CONTROL_AF_MODE,
                            CaptureRequest.CONTROL_AF_MODE_CONTINUOUS_VIDEO);
                } else {
                    mPreviewRequestBuilder.set(CaptureRequest.CONTROL_AF_MODE,
                            CaptureRequest.CONTROL_AF_MODE_CONTINUOUS_PICTURE);
                }
            }
        } else {
            mPreviewRequestBuilder.set(CaptureRequest.CONTROL_AF_MODE,
                    CaptureRequest.CONTROL_AF_MODE_OFF);
        }
        mAFRegions = AutoFocusHelper.getZeroWeightRegion();
        mAERegions = AutoFocusHelper.getZeroWeightRegion();
        mPreviewRequestBuilder.set(CaptureRequest.CONTROL_AF_REGIONS, mAFRegions);
        mPreviewRequestBuilder.set(CaptureRequest.CONTROL_AE_REGIONS, mAERegions);
    }

    /**
     * Updates the internal state of manual focus.
     */
    void updateManualFocus(float x, float y) {
        @SuppressWarnings("ConstantConditions")
        int sensorOrientation = mCameraCharacteristics.get(CameraCharacteristics.SENSOR_ORIENTATION);
        mAFRegions = AutoFocusHelper.afRegionsForNormalizedCoord(x, y, mCropRegion, sensorOrientation);
        mAERegions = AutoFocusHelper.aeRegionsForNormalizedCoord(x, y, mCropRegion, sensorOrientation);
        mPreviewRequestBuilder.set(CaptureRequest.CONTROL_AF_REGIONS, mAFRegions);
        mPreviewRequestBuilder.set(CaptureRequest.CONTROL_AE_REGIONS, mAERegions);
        mPreviewRequestBuilder.set(CaptureRequest.CONTROL_AF_MODE, CaptureRequest.CONTROL_AF_MODE_AUTO);
    }

    void setManualFocusAt(int x, int y) {
        int mDisplayOrientation = getActivity().getWindowManager().getDefaultDisplay().getRotation();
        float points[] = new float[2];
        points[0] = (float) x / mTextureView.getWidth();
        points[1] = (float) y / mTextureView.getHeight();
        Matrix rotationMatrix = new Matrix();
        rotationMatrix.setRotate(mDisplayOrientation, 0.5f, 0.5f);
        rotationMatrix.mapPoints(points);
        if (mPreviewRequestBuilder != null) {
            mIsManualFocusing = true;
            updateManualFocus(points[0], points[1]);
            if (mPreviewSession != null) {
                try {
                    mPreviewRequestBuilder.set(CaptureRequest.CONTROL_AF_TRIGGER,
                            CaptureRequest.CONTROL_AF_TRIGGER_START);
                    mPreviewSession.capture(mPreviewRequestBuilder.build(), null, mBackgroundHandler);
                    mPreviewRequestBuilder.set(CaptureRequest.CONTROL_AF_TRIGGER,
                            CaptureRequest.CONTROL_AF_TRIGGER_IDLE);
                    mPreviewSession.setRepeatingRequest(mPreviewRequestBuilder.build(),
                            null, mBackgroundHandler);
                } catch (CameraAccessException | IllegalStateException e) {
                    Log.e(TAG, "Failed to set manual focus.", e);
                }
            }
            resumeAutoFocusAfterManualFocus();
        }
    }

    private final Runnable mAutoFocusRunnable = new Runnable() {
        @Override
        public void run() {
            if (mPreviewRequestBuilder != null) {
                mIsManualFocusing = false;
                updateAutoFocus();
                if (mPreviewSession != null) {
                    try {
                        mPreviewSession.setRepeatingRequest(mPreviewRequestBuilder.build(),
                                mCaptureCallback, mBackgroundHandler);
                    } catch (CameraAccessException e) {
                        Log.e(TAG, "Failed to set manual focus.", e);
                    }
                }
            }
        }
    };

    private void resumeAutoFocusAfterManualFocus() {
        mBackgroundHandler.removeCallbacks(mAutoFocusRunnable);
        mBackgroundHandler.postDelayed(mAutoFocusRunnable, CameraConstants.FOCUS_HOLD_MILLIS);
    }

    /**
     * Initiate a still image capture.
     */
    public void takePicture() {
        if (!mIsManualFocusing && mAutoFocus && mAutoFocusSupported) {
            Log.i(TAG, "takePicture lockFocus");
            capturePictureWhenFocusTimeout(); //Sometimes, camera do not focus in some devices.
            lockFocus();
        } else {
            Log.i(TAG, "takePicture captureStill");
            captureStillPicture();
        }
    }

    /**
     * Capture picture when auto focus timeout
     */
    private void capturePictureWhenFocusTimeout() {
        if (mBackgroundHandler != null) {
            mBackgroundHandler.sendEmptyMessageDelayed(MSG_CAPTURE_PICTURE_WHEN_FOCUS_TIMEOUT,
                    CameraConstants.AUTO_FOCUS_TIMEOUT_MS);
        }
    }

    /**
     * Remove capture message, because auto focus work correctly.
     */
    private void removeCaptureMessage() {
        if (mBackgroundHandler != null) {
            mBackgroundHandler.removeMessages(MSG_CAPTURE_PICTURE_WHEN_FOCUS_TIMEOUT);
        }
    }

    /**
     * Lock the focus as the first step for a still image capture.
     */
    private void lockFocus() {
        try {
            // This is how to tell the camera to lock focus.
            mPreviewRequestBuilder.set(CaptureRequest.CONTROL_AF_TRIGGER,
                    CameraMetadata.CONTROL_AF_TRIGGER_START);
            // Tell #mCaptureCallback to wait for the lock.
            mState = STATE_WAITING_LOCK;
            mPreviewSession.capture(mPreviewRequestBuilder.build(), mCaptureCallback, mBackgroundHandler);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    /**
     * Unlock the focus. This method should be called when still image capture sequence is
     * finished.
     */
    private void unlockFocus() {
        try {
            // Reset the auto-focus trigger
            mPreviewRequestBuilder.set(CaptureRequest.CONTROL_AF_TRIGGER,
                    CameraMetadata.CONTROL_AF_TRIGGER_CANCEL);
            mPreviewSession.capture(mPreviewRequestBuilder.build(), mCaptureCallback,
                    mBackgroundHandler);

            updateAutoFocus();
            updateFlash(mPreviewRequestBuilder);
            // After this, the camera will go back to the normal state of preview.
            mState = STATE_PREVIEW;
            mPreviewRequestBuilder.set(CaptureRequest.CONTROL_AF_TRIGGER,
                    CaptureRequest.CONTROL_AF_TRIGGER_IDLE);
//            mPreviewSession.setRepeatingRequest(mPreviewRequest, mCaptureCallback,
//                    mBackgroundHandler);
            updatePreview();
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    /**
     * Run the precapture sequence for capturing a still image. This method should be called when
     * we get a response in {@link #mCaptureCallback} from {@link #lockFocus()}.
     */
    private void runPrecaptureSequence() {
        try {
            // This is how to tell the camera to trigger.
            mPreviewRequestBuilder.set(CaptureRequest.CONTROL_AE_PRECAPTURE_TRIGGER,
                    CaptureRequest.CONTROL_AE_PRECAPTURE_TRIGGER_START);
            // Tell #mCaptureCallback to wait for the precapture sequence to be set.
            mState = STATE_WAITING_PRECAPTURE;
            mPreviewSession.capture(mPreviewRequestBuilder.build(), mCaptureCallback, mBackgroundHandler);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    /**
     * Capture a still picture. This method should be called when we get a response in
     * {@link #mCaptureCallback} from both {@link #lockFocus()}.
     */
    private void captureStillPicture() {
        try {
            removeCaptureMessage();
            final Activity activity = getActivity();
            if (null == activity || null == mCameraDevice) {
                return;
            }
            // This is the CaptureRequest.Builder that we use to take a picture.
            final CaptureRequest.Builder captureBuilder =
                    mCameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_STILL_CAPTURE);
            captureBuilder.addTarget(mImageReader.getSurface());

            // Use the same AE and AF modes as the preview.
//            captureBuilder.set(CaptureRequest.CONTROL_AF_MODE,
//                CaptureRequest.CONTROL_AF_MODE_CONTINUOUS_PICTURE);
//            updateAutoFocus();
            updateFlash(captureBuilder);

            // Orientation
            int rotation = activity.getWindowManager().getDefaultDisplay().getRotation();
            captureBuilder.set(CaptureRequest.JPEG_ORIENTATION, getOrientation(rotation));
            captureBuilder.set(CaptureRequest.SCALER_CROP_REGION, calculateZoomRect());
            CameraCaptureSession.CaptureCallback CaptureCallback
                    = new CameraCaptureSession.CaptureCallback() {

                @Override
                public void onCaptureCompleted(@NonNull CameraCaptureSession session,
                                               @NonNull CaptureRequest request,
                                               @NonNull TotalCaptureResult result) {
                    unlockFocus();
                }
            };

            mPreviewSession.stopRepeating();
            mPreviewSession.capture(captureBuilder.build(), CaptureCallback, mBackgroundHandler);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    /**
     * Retrieves the JPEG orientation from the specified screen rotation.
     *
     * @param rotation The screen rotation.
     * @return The JPEG orientation (one of 0, 90, 270, and 360)
     */
    private int getOrientation(int rotation) {
        // Sensor orientation is 90 for most devices, or 270 for some devices (eg. Nexus 5X)
        // We have to take that into account and rotate JPEG properly.
        // For devices with orientation of 90, we simply return our mapping from DEFAULT_ORIENTATIONS.
        // For devices with orientation of 270, we need to rotate the JPEG 180 degrees.
        return (DEFAULT_ORIENTATIONS.get(rotation) + mSensorOrientation + 270) % 360;
    }


    private String getPictureFilePath(Context context) {
        final File dir = context.getExternalFilesDir(null);
        return (dir == null ? "" : (dir.getAbsolutePath() + "/"))
                + System.currentTimeMillis() + ".jpg";
    }

    /**
     * Compares two {@code Size}s based on their areas.
     */
    private static class CompareSizesByArea implements Comparator<Size> {

        @Override
        public int compare(Size lhs, Size rhs) {
            // We cast here to ensure the multiplications won't overflow
            return Long.signum((long) lhs.getWidth() * lhs.getHeight() -
                    (long) rhs.getWidth() * rhs.getHeight());
        }

    }

    private void updateZoom(float newZoom) {
        newZoom = clampZoom(newZoom);
        if (CameraUtil.checkFloatEqual(zoom, newZoom)) return;
        zoom = newZoom;
        applyZoom();
    }

    private float clampZoom(float zoom) {
        return CameraUtil.clamp(zoom, 1.f, maxZoom);
    }

    private void applyZoom() {
        Rect zoomRect = calculateZoomRect();
        mPreviewRequestBuilder.set(CaptureRequest.SCALER_CROP_REGION, zoomRect);
    }

    private Rect calculateZoomRect() {
        final Rect origin = mCameraCharacteristics.get(CameraCharacteristics.SENSOR_INFO_ACTIVE_ARRAY_SIZE);
        if (origin == null) return null;
        if (CameraUtil.checkFloatEqual(zoom, 1.f) || zoom < 1.f) return origin;

        int xOffset = (int) (((1 - 1 / zoom) / 2) * (origin.right - origin.left));
        int yOffset = (int) (((1 - 1 / zoom) / 2) * (origin.bottom - origin.top));

        return new Rect(xOffset, yOffset, origin.right - xOffset, origin.bottom - yOffset);
    }

    private void updatePreview() {
        try {
            mPreviewSession.setRepeatingRequest(mPreviewRequestBuilder.build(), mCaptureCallback, null);
        } catch (CameraAccessException e) {
        }
    }

    @Override
    public void onDestroy() {
        Log.d("==VC", "DESTROY");
        if (msAdaptor != null) {
            msAdaptor.destroyJanus();
            msAdaptor.hangUp();
            msAdaptor.disconnectRoomIDSocket();
            msAdaptor.sendEndCallEvent("user_ended_call", "ended", true);
            msAdaptor.disconnectOdSocket();
        }
        msAdaptor = null;
        super.onDestroy();
    }
}