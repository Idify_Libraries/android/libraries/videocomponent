package com.idfy.videocomponent.util;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

public class Utility {

    public static Uri getUri(Context context, Bitmap bitmap) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String filename = "myimage"+System.currentTimeMillis();
        String path = MediaStore.Images.Media.insertImage(context.getContentResolver(), bitmap, filename, null);

        String pathh = getRealPathFromURI(context, Uri.parse(path));
        return Uri.parse(path);
    }
    public static String getUriPath(Context context, Bitmap bitmap) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String filename = "myimage"+System.currentTimeMillis();
        String path = MediaStore.Images.Media.insertImage(context.getContentResolver(), bitmap, filename, null);

        String pathh = getRealPathFromURI(context, Uri.parse(path));
        return pathh;
    }

    public static String getRealPathFromURI(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = { MediaStore.Images.Media.DATA };
            cursor = context.getContentResolver().query(contentUri,  proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }


    public static Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        try {
            Bitmap bitmap =  Bitmap.createScaledBitmap(image, width, height, true);
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 50, out);
            return BitmapFactory.decodeStream(new ByteArrayInputStream(out.toByteArray()));

        } catch (Exception e) {
            return null;
        }
    }
}
