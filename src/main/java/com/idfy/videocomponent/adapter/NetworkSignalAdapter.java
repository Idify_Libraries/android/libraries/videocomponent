package com.idfy.videocomponent.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.idfy.msadaptor.model.NetworkSignalModel;
import com.idfy.videocomponent.R;

import java.util.List;

public class NetworkSignalAdapter extends RecyclerView.Adapter<NetworkSignalAdapter.NetworkSignalViewHolder>{
    private List<NetworkSignalModel> networkSignalModelList;


    public NetworkSignalAdapter(List<NetworkSignalModel> networkSignalModelList) {
        this.networkSignalModelList = networkSignalModelList;
    }

    @NonNull
    @Override
    public NetworkSignalViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.network_signal_layout, parent, false);
        return new NetworkSignalViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull NetworkSignalViewHolder holder, int position) {
        NetworkSignalModel signalModel = networkSignalModelList.get(position);
        holder.label.setText(signalModel.getLabel());
        holder.label2.setText(signalModel.getLabel2());
        holder.label3.setText(signalModel.getLabel3());

    }

    @Override
    public int getItemCount() {
        return (networkSignalModelList.size() == 0) ? 0 : networkSignalModelList.size();
    }

    class NetworkSignalViewHolder extends RecyclerView.ViewHolder{
        private TextView label;
        private TextView label2;
        private TextView label3;

        public NetworkSignalViewHolder(@NonNull View itemView) {
            super(itemView);
            label = itemView.findViewById(R.id.label);
            label2 = itemView.findViewById(R.id.label2);
            label3 = itemView.findViewById(R.id.label3);
        }
    }
}
